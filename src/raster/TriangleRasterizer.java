package raster;

import transforms.Col;
import transforms.Point3D;
import transforms.Vec3D;

import java.awt.*;

public class TriangleRasterizer {
    private final ZBuffer zBuffer;
    private int width, height;

    public TriangleRasterizer(ZBuffer zBuffer) {
        this.zBuffer = zBuffer;
        this.width = zBuffer.getWidth();
        this.height = zBuffer.getHeight();
    }

    public void rasterize(Point3D p1, Point3D p2, Point3D p3){
        //pointy jsou v ndc -1;1
        Vec3D a=transformToWindow(p1);
        Vec3D b=transformToWindow(p2);
        Vec3D c=transformToWindow(p3);

        Graphics g= zBuffer.getImageBuffer().getGraphics();
        g.setColor(new Color(0xffff00));
        g.drawLine((int)a.x, (int) a.y, (int) b.x, (int) b.y);
        g.drawLine((int)b.x, (int) b.y, (int) c.x, (int) c.y);
        g.drawLine((int)c.x, (int) c.y, (int) a.x, (int) a.y);


        // todo: seřadit abc podle y (od nejmensiho po nejvetsi? idk) linearni interpolace slide +- 130 v prezentaci
        if(a.y<b.y){
            double tmpX=a.x;
            double tmpY= a.y;
            a.x = b.x;
            a.y = b.y;
            b.x = tmpX;
            b.y= tmpY;
        }
        if (b.y<c.y){
            double tmpX=b.x;
            double tmpY= b.y;
            b.x = c.x;
            b.y = c.y;
            c.x = tmpX;
            c.y= tmpY;
        }
        if (a.y<c.y){
            double tmpX=a.x;
            double tmpY= a.y;
            a.x = c.x;
            a.y = c.y;
            c.x = tmpX;
            c.y= tmpY;
        }

        for (int y= (int)a.y; y<b.y; y++){
            //interpolacni koeficient- podel hrany AB. je vzdy zaporny?
            double s1=(y-a.y)/(b.y-a.y);
            int x1= (int)((1-s1)*a.x+s1*b.x);

            int z= (int) a.z;

            double s3=(z-a.z)/(b.z-a.z);
            int z1= (int)((1-s3)*a.z+s3*b.z);

            //interpolacni koeficient podel hrany AC
            double s2 = (y - a.y) / (c.y - a.y);
            int x2 = (int) ((1 - s2) * a.x + s2 * c.x);

            double s4 = (z - a.z) / (c.z - a.z);
            int z2 = (int) ((1 - s4) * a.z + s4 * c.z);

            // todo interpolaci pro Z


            for (int x=x1; x<=x2; x++){
                zBuffer.drawWithTest(x, y, z, new Col(0xff0000));
            }
            z++;
        }

        for (int y= (int) b.y; y<c.y; y++){
            double s3=(y-b.y)/(c.y-b.y);
            int x3= (int)((1-s3)*b.x+s3*c.x);

            int z= (int) b.z;

            double s1=(z-b.z)/(c.z-b.z);
            int z1= (int)((1-s1)*b.z+s1*c.z);

            //interpolacni koeficient podel hrany AB
            double s4 = (y - c.y) / (a.y - c.y);
            int x4 = (int) ((1 - s4) * b.x + s4 * a.x);

            double s2 = (z - c.z) / (a.z - c.z);
            int z2 = (int) ((1 - s2) * b.z + s2 * a.z);

            // todo interpolaci pro Z

            for (int x=x3; x<=x4; x++){
                zBuffer.drawWithTest(x, y, z, new Col(0xff0000));
            }
            z++;
        }



        // todo interpolaci pro druhou polovinu trojuhelniku + vykresleni trojuhelniku. myslim ze je chyba v drawWithTest,
        // protoze to nevybarvuje


        //todo otestovat 2 protinajici se trojuhleniky
    }
    // popřemýšlet kde to bude v budoucnu
    private Vec3D transformToWindow(Point3D p){
        return p.ignoreW().mul(new Vec3D(1,-1,1)).add(new Vec3D(1,1,0)).mul(new Vec3D((width-1)/2.,(height-1)/2., 1));
    }
}
