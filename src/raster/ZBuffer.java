package raster;

import transforms.Col;

public class ZBuffer {
    private final DepthBuffer depthBuffer;
    private final ImageBuffer imageBuffer;

    public ZBuffer(ImageBuffer imageBuffer) {
        this.imageBuffer = imageBuffer;
        this.depthBuffer = new DepthBuffer(imageBuffer.getWidth(), imageBuffer.getHeight());
    }

    public void drawWithTest(int x, int y, double z, Col color){

        // TODO: Implementace zbufferu jako takového
        // Načtu hodnotu z depthBUfferu na pozici x,y
        // Načetnou hodnotu porovnám s Z, které vstoupilo do metody (nové)
        // pokud, je nové < než staré
        // tak -> obravím a nastavím nové z v paměti hloubky


        //set value if new z is greater than old

        if (!depthBuffer.isValid(x,y)) return;
        else {
            double oldZ= depthBuffer.getValue(x, y);
            if (z < oldZ) {
                depthBuffer.setValue(x, y, z);
                imageBuffer.setValue(x, y, color);
            }
            else return;
        }
    }
    public int getWidth(){
        return imageBuffer.getWidth();
    }
    public int getHeight(){
        return imageBuffer.getHeight();
    }

    public ImageBuffer getImageBuffer() {
        return imageBuffer;
    }
}
