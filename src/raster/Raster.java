package raster;

public interface Raster<E> {

    void clear();

    void setClearValue(E value);

    int getWidth();

    int getHeight();

    E getValue(int x, int y);

    void setValue(int x, int y, E value);

    //check if x and y are in width and height
    default boolean isValid(int x, int y) {
        int width= getWidth();
        int height= getHeight();
        if (x > width || y > height)
            return false;
        return true;
    }
}
