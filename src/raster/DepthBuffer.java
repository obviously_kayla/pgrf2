package raster;

import java.lang.reflect.Array;

public class DepthBuffer implements Raster<Double>{

    private double clearValue=1;
    private final int width,height;
    private final double [][] buffer;

    public DepthBuffer(int width, int height) {
        this.width = width;
        this.height = height;
        this.buffer= new double[width][height];
    }



    @Override
    public void clear() {
        // nastaví celý buffer na clearValue
        for (int i=0; i<width;i++){
            for (int j=0; j<height;j++){
                buffer[i][j]=clearValue;
            }
        }

    }

    @Override
    public void setClearValue(Double value) {
        //setter pro clearValue
        buffer[this.width][this.height]= clearValue;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public Double getValue(int x, int y) {
        //todo: optional
        if (isValid(x,y))
            return buffer[x][y];
        return null;
    }

    @Override
    public void setValue(int x, int y, Double value) {
        if (isValid(x, y))
            buffer[x][y]= value;
    }


}
